# Examples for m2m jupyterhub notebook

- [`genomes`](genomes) directory of genomes that can be submitted to `m2m` and `mpwt`.
- [`tasks`](tasks) example tasks that can be submitted to the TES endpoint.

## Copy these examples into a Jupyter server

Generate a link for [nbgitpuller](https://jupyterhub.github.io/nbgitpuller/) using the [**link generator form**](https://jupyterhub.github.io/nbgitpuller/link.html?repo=https://gitlab.inria.fr/alcyone/m2m-examples&branch=main&app=jupyterlab)
